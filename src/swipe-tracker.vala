double easeOutCubic (double t) {
	double p = t - 1;
	return p * p * p + 1;
}

public class Mockup.SwipeTracker : Object {
	private const double SWIPE_SPEED = 400;
	private const double SCROLL_MULTIPLIER = 10;

	private const int64 MIN_ANIMATION_DURATION = 100;
	private const int64 MAX_ANIMATION_DURATION = 400;
	private const double CANCEL_AREA = 0.5;
	private const double VELOCITY_THRESHOLD = 0.001;
	private const double DURATION_MULTIPLIER = 3;

	public enum Direction {
		FORWARD = -1,
		BACK = 1,
	}

	private enum State {
		NONE,
		PENDING,
		SCROLLING,
		DECELERATING,
	}

	public signal void begin (Direction direction);
	public signal void update (double progress);
	public signal void end (bool cancel);

	public Gtk.Widget widget { get; construct; }
	public Gtk.Orientation orientation { get; construct; }

	private Gtk.EventControllerScroll scroll_controller;
	private Gtk.GestureDrag touch_gesture;
	private Gtk.GestureDrag three_touch_gesture;

	private State state;

	private uint32 prev_time;
	private double velocity;

	private uint tick_cb_id;
	private int64 start_time;
	private int64 end_time;

	private double progress;
	private double start_progress;
	private double end_progress;
	private bool cancelled;

	private double prev_offset;

	public bool can_swipe_back { get; set; }
	public bool can_swipe_forward { get; set; }

	public SwipeTracker (Gtk.Widget widget, Gtk.Orientation orientation) {
		Object (widget: widget, orientation: orientation);
	}

	construct {
		reset ();

		scroll_controller = new Gtk.EventControllerScroll (widget, Gtk.EventControllerScrollFlags.BOTH_AXES);
		scroll_controller.propagation_phase = Gtk.PropagationPhase.CAPTURE;

		touch_gesture = Object.new (typeof (Gtk.GestureDrag),
		                            "widget", widget,
		                            "n-points", 1,
		                            null) as Gtk.GestureDrag;
		touch_gesture.propagation_phase = Gtk.PropagationPhase.CAPTURE;

		three_touch_gesture = Object.new (typeof (Gtk.GestureDrag),
		                                  "widget", widget,
		                                  "n-points", 3,
		                                  null) as Gtk.GestureDrag;
		touch_gesture.propagation_phase = Gtk.PropagationPhase.CAPTURE;

		scroll_controller.scroll_begin.connect (() => gesture_begin ());
		scroll_controller.scroll.connect ((dx, dy) => {
			gesture_update ((is_vertical () ? -dy : -dx) * SCROLL_MULTIPLIER / SWIPE_SPEED);
		});
		scroll_controller.scroll_end.connect (() => gesture_end ());

		three_touch_gesture.drag_begin.connect (() => gesture_begin ());
		three_touch_gesture.drag_update.connect ((offset_x, offset_y) => {
			double offset = 0;
			if (is_vertical ())
				offset = offset_y / SWIPE_SPEED;
			else
				offset = offset_x / SWIPE_SPEED;

			gesture_update (offset - prev_offset);
			prev_offset = offset;
		});
		three_touch_gesture.drag_end.connect ((offset_x, offset_y) => gesture_end ());


		touch_gesture.drag_begin.connect (() => gesture_begin ());
		touch_gesture.drag_update.connect ((offset_x, offset_y) => {
			double offset = 0;
			if (is_vertical ())
				offset = offset_y / (double) widget.get_allocated_height ();
			else
				offset = offset_x / (double) widget.get_allocated_width ();

			gesture_update (offset - prev_offset);
			prev_offset = offset;
		});
		touch_gesture.drag_end.connect ((offset_x, offset_y) => gesture_end ());
	}

	public void set_initial_progress (double progress) {
		this.progress = progress;
	}

	private void reset () {
		state = State.NONE;

		if (tick_cb_id != 0) {
			widget.remove_tick_callback (tick_cb_id);
			tick_cb_id = 0;
		}

		prev_offset = 0;

		progress = 0;
		start_progress = 0;
		end_progress = 0;

		start_time = 0;
		end_time = 0;
		prev_time = 0;
		velocity = 0;
		cancelled = false;
	}

	private bool is_vertical () {
		return (orientation == Gtk.Orientation.VERTICAL);
	}

	private void gesture_begin () {
		if (state == State.SCROLLING || state == State.PENDING)
			return;

		prev_time = Gtk.get_current_event ().get_time ();

		if (state == State.DECELERATING) {
			widget.remove_tick_callback (tick_cb_id);
			tick_cb_id = 0;
			prev_offset = 0;
			state = State.SCROLLING;
		} else
			state = State.PENDING;
	}

	private void gesture_update (double delta) {
		if (state != State.PENDING && state != State.SCROLLING)
			return;

		var overshoot = (progress + delta > 0 && !can_swipe_back) || (progress + delta < 0 && !can_swipe_forward);

		if (progress * (progress + delta) <= 0) {
			if (state == State.SCROLLING) {
				update (0);
				end (true);
			}

			state = State.PENDING;

			if (!overshoot) {
				begin (progress + delta > 0 ? Direction.BACK : Direction.FORWARD);
				update (0);
				state = State.SCROLLING;
			}
		}

		var time = Gtk.get_current_event ().get_time ();

		if (overshoot) {
			progress = 0;
			velocity = 0;
		} else {
			progress += delta;

			if (time != prev_time)
				velocity = delta / (time - prev_time);

			var max_progress = (progress > 0) ? 1 : 0;
			var min_progress = (progress < 0) ? -1 : 0;

			progress = progress.clamp (min_progress, max_progress);

			update (progress);
		}

		prev_time = time;
	}

	private bool should_cancel () {
		if (progress == 0)
			return true;

		if (progress > 0 && !can_swipe_back)
			return true;

		if (progress < 0 && !can_swipe_forward)
			return true;

		if (velocity * progress < 0)
			return true;

		return progress.abs () < CANCEL_AREA && velocity.abs () < VELOCITY_THRESHOLD;
	}

	private void gesture_end () {
		if (state == State.PENDING) {
			reset ();
			return;
		}

		if (state != State.SCROLLING)
			return;

		cancelled = should_cancel ();

		start_progress = progress;
		if (cancelled)
			end_progress = 0;
		else
			end_progress = (progress > 0) ? 1 : -1;

		var duration = MAX_ANIMATION_DURATION;

		if ((end_progress - progress) * velocity > 0) {
			duration = (int64) ((progress - end_progress) / velocity * DURATION_MULTIPLIER).abs ();
			duration = duration.clamp (MIN_ANIMATION_DURATION, MAX_ANIMATION_DURATION);
		}

		start_animation (duration);
	}

	private void start_animation (int64 duration) {
		state = State.DECELERATING;

		if (duration <= 0 || (velocity == 0 && progress == end_progress)) {
			end_animation ();
			return;
		}

		start_time = widget.get_frame_clock ().get_frame_time () / 1000;
		end_time = start_time + duration;

		tick_cb_id = widget.add_tick_callback (tick_cb);
	}

	private bool tick_cb (Gtk.Widget widget, Gdk.FrameClock frame_clock) {
		assert (state == State.DECELERATING);
		assert (end_time > start_time);

		int64 frame_time = frame_clock.get_frame_time () / 1000;

		double animation_progress = (double) (frame_time - start_time) / (double) (end_time - start_time);
		if (animation_progress > 1)
			animation_progress = 1;

		progress = start_progress + (end_progress - start_progress) * easeOutCubic (animation_progress);
		update (progress);

		if (frame_time >= end_time) {
			tick_cb_id = 0;
			end_animation ();
			return Source.REMOVE;
		}

		return Source.CONTINUE;
	}

	private void end_animation () {
		progress = end_progress;

		end (cancelled);

		reset ();
	}

	private void animate (Direction direction, double p, int64 duration) {
		if (state != State.NONE)
			return;

		begin (direction);
		update (0);

		cancelled = false;

		start_progress = progress;
		end_progress = p;

		start_animation (duration);
	}

	public void animate_back (int64 duration) {
		if (!can_swipe_back)
			return;

		animate (Direction.BACK, 1, duration);
	}

	public void animate_forward (int64 duration) {
		if (!can_swipe_forward)
			return;

		animate (Direction.FORWARD, -1, duration);
	}

}