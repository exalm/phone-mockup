public class Mockup.PhoneScreen : Gtk.DrawingArea {
//360 654
	private double _progress;
	public double progress {
		get { return _progress; }
		set {
			_progress = value;
			queue_draw ();
		}
	}

	private Cairo.Surface topbar;
	private Cairo.Surface appdrawer;
	private Cairo.Surface apps;
	private Cairo.Surface wallpaper;

	private double topbar_height;
	private double bottombar_height;
	private double appdrawer_partial_height;

	private Rsvg.Rectangle window_rect;

	construct {
		var topbar_handle = load_svg ("topbar");
		var apps_handle = load_svg ("apps");
		var appdrawer_handle = load_svg ("appdrawer");

		topbar_height = get_rect (topbar_handle, null).height;

		bottombar_height = 40;
		appdrawer_partial_height = 183 - bottombar_height;
		window_rect = get_rect (apps_handle, "#window");

		topbar = cache_svg (topbar_handle);
		appdrawer = cache_svg (appdrawer_handle);
		apps = cache_svg (apps_handle);
		wallpaper = load_png ("wallpaper");

		set_size_request ((int) Math.round (window_rect.width), (int) Math.round (topbar_height + window_rect.height + bottombar_height));
	}

	private Cairo.Surface load_png (string name) {
		try {
			var pixbuf = new Gdk.Pixbuf.from_resource ("/org/gnome/Mockup/%s.png".printf (name));

			return Gdk.cairo_surface_create_from_pixbuf (pixbuf, 1, get_window ());
		}
		catch (Error e) {
			critical ("Cannot load png: %s", name);
			return new Cairo.ImageSurface (Cairo.Format.ARGB32, 256, 256);
		}
	}

	private Cairo.Surface cache_svg (Rsvg.Handle handle) {
		var rect = get_rect (handle, null);
		var surface = new Cairo.ImageSurface (Cairo.Format.ARGB32, (int) rect.width, (int) rect.height);
		var context = new Cairo.Context (surface);

		handle.render_cairo (context);

		return surface;
	}

	private Rsvg.Handle load_svg (string name) {
		try {
			var bytes = resources_lookup_data ("/org/gnome/Mockup/%s.svg".printf (name), ResourceLookupFlags.NONE);
			var data = bytes.get_data ();

			return new Rsvg.Handle.from_data (data);
		}
		catch (Error e) {
			critical ("Cannot load svg: %s", name);
			return new Rsvg.Handle ();
		}
	}

	private Rsvg.Rectangle get_rect (Rsvg.Handle handle, string? sub) {
		Rsvg.Rectangle rect = {};
		if (!handle.get_geometry_sub (out rect, null, sub))
			assert_not_reached ();

		return rect;
	}

	public override bool draw (Cairo.Context cr) {
		cr.rectangle (0, 0, get_allocated_width (), topbar_height + bottombar_height + window_rect.height);
		cr.clip ();

//		cr.set_source_rgb(1, 0, 0);
		cr.set_source_surface (wallpaper, 0, 0);
		cr.paint ();

		cr.save ();
		cr.translate (0, topbar_height);

		double window_offset = 1 - double.min (progress, 1);
		double scale = 0.6 + 0.4 * window_offset;

		cr.translate (window_rect.width / 2, window_rect.height / 2 - appdrawer_partial_height - bottombar_height);

		cr.scale (scale, scale);

		cr.translate (-window_rect.x - window_rect.width / 2, -window_rect.y - window_rect.height / 2 + appdrawer_partial_height + bottombar_height);

		cr.set_source_surface (apps, 0, 0);
		cr.paint ();
		cr.restore();

		cr.save ();
		cr.translate (0, topbar_height + window_rect.height);
		double offset = 0;
		if (progress <= 1)
			offset = -appdrawer_partial_height * progress;
		else
			offset = -appdrawer_partial_height + (window_rect.height - appdrawer_partial_height) * (1 - progress);

		cr.translate (0, offset);

		cr.set_source_surface (appdrawer, 0, 0);
		cr.paint ();

		cr.restore();

		cr.set_source_surface (topbar, 0, 0);
		cr.paint ();

		return false;
	}
}
